package interfaz;

import java.util.Random;

import mochila.GeneradorRandom;
import mochila.Individuo;
import mochila.Instancia;
import mochila.Objeto;
import mochila.Poblacion;

public class MainClass
{
	public static void main(String[] args)
	{
		Instancia instancia = aleatoria(200);
		Individuo.setGenerador(new GeneradorRandom());
		Poblacion poblacion = new Poblacion(instancia);
		Observador observador = new ObservadorPorConsola(poblacion);

		poblacion.registrarObservador(observador);
		poblacion.simular();
	}

	private static Instancia aleatoria(int n)
	{
		Random random = new Random(0);

		Instancia instancia = new Instancia();
		instancia.setCapacidad(100);
		
		for(int i=0; i<n; ++i)
		{
			int peso = random.nextInt(10) + 1;
			int benef = random.nextInt(10) + 1;

			instancia.agregarObjeto(new Objeto("Obj" + i, peso, benef));
		}
		return instancia;
	}
}
