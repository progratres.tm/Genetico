package mochila;

import java.util.Random;

public class GeneradorRandom implements Generador
{
	Random _random = new Random();
	
	public boolean nextBoolean()
	{
		return _random.nextBoolean();
	}
	
	public int nextInt(int n)
	{
		return _random.nextInt(n);
	}
	
}
