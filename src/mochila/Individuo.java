package mochila;

public class Individuo 
{
	private Instancia _instancia;
	private boolean[] _bits;
	private static Generador _generador = null;
	
	public static Individuo aleatorio(Instancia instancia)
	{
		Individuo ret = new Individuo(instancia);
		
		for (int i=0; i<instancia.cantidadDeObjetos();++i)
			ret._bits[i] = _generador.nextBoolean();
		
		return ret;
	}
	
	public static Individuo vacio(Instancia instancia)
	{
		Individuo ret = new Individuo(instancia);
		
		return ret;
	}
	
	public static Individuo completo(Instancia instancia)
	{
		Individuo ret = new Individuo(instancia);
		for (int i=0; i<instancia.cantidadDeObjetos();++i)
			ret.set(i, true);
		
		return ret;
	}
	
	static Individuo prefijado(Instancia instancia, String bits)
	{
		// C�digo defensivo
		if( bits != null && bits.length() != instancia.cantidadDeObjetos() )
			throw new IllegalArgumentException("El string de bits es incorrecto! bits = " + bits);
		
		Individuo ret = Individuo.vacio(instancia);
		
		for (int i = 0; i < instancia.cantidadDeObjetos(); ++i)
		{
			ret.set(i, bits.charAt(i) == '1');
		}
		
		return ret;
	}
	
	private Individuo(Instancia instancia)
	{
		if( _generador == null )
			throw new IllegalArgumentException("Individuo no tiene un generador!");

		_instancia = instancia;
		_bits = new boolean[_instancia.cantidadDeObjetos()];		
	}
	
	public void setearGenerador(Generador gen)
	{
		_generador = gen;
	}
	
	public void mutar()
	{
		int i = _generador.nextInt(_instancia.cantidadDeObjetos());
		_bits[i] = !_bits[i];
	}
	
	public Individuo[] combinar(Individuo that)
	{
		int k = _generador.nextInt(_instancia.cantidadDeObjetos());
		
		Individuo hijo1 = Individuo.vacio(_instancia);
		Individuo hijo2 = Individuo.vacio(_instancia);
		
		for (int i=0; i<k; ++i)
		{
			hijo1.set(i, this.get(i));
			hijo2.set(i, that.get(i));
		}
		
		for (int i=k; i<_instancia.cantidadDeObjetos();++i)
		{
			hijo1.set(i, that.get(i));
			hijo2.set(i, this.get(i));
		}
		
		return new Individuo[] {hijo1, hijo2};
	}
	
	// El fitness es negativo si no cumple la capacidad de la mochila
	public double fitness()
	{
		if( this.peso() <= _instancia.capacidad() )
			return this.beneficio();
		else
			return - this.peso() + _instancia.capacidad();		
	}
	
	
	// Setter para el generador de n�meros pseudoaleatorios
	public static void setGenerador(Generador random)
	{
		_generador = random;
	}

	// Peso y beneficio de un individuo
	public double peso()
	{
		// TODO: Testear!
		double ret = 0;
		for(int i=0; i<_instancia.cantidadDeObjetos(); ++i) if( get(i) == true )
			ret += _instancia.objeto(i).peso();
		
		return ret;
	}
	
	public double beneficio()
	{
		//TODO: Testear!
		double ret = 0;
		for(int i=0; i<_instancia.cantidadDeObjetos(); ++i) if( get(i) == true )
			ret += _instancia.objeto(i).beneficio();
		
		return ret;
	}
	
	void set(int i, boolean nuevo)
	{
		this._bits[i] = nuevo;
	}
	
	boolean get(int i)
	{
	 return _bits[i];
	}

}
