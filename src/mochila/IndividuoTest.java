package mochila;

import static org.junit.Assert.*;

import org.junit.Test;

public class IndividuoTest 
{

	@Test
	public void mutarTest()
	{
		Individuo.setGenerador(new GeneradorPrefijado(3));
		Individuo ind = Individuo.prefijado(defecto(), "11001");

		ind.mutar();
		sonIguales("11011", ind);
	}
	
	@Test
	public void mutarAlRevesTest()
	{
		Individuo.setGenerador(new GeneradorPrefijado(3));
		Individuo ind = Individuo.prefijado(defecto(), "11011");

		ind.mutar();
		sonIguales("11001", ind);
	}	
	
	@Test
	public void combinarTest()
	{
		Individuo.setGenerador(new GeneradorPrefijado(2));
		Individuo padre1 = Individuo.prefijado(defecto(), "11001");
		Individuo padre2 = Individuo.prefijado(defecto(), "01111");
		Individuo[] hijos = padre1.combinar(padre2);
		
		assertEquals(2, hijos.length);
		sonIguales("11111", hijos[0]);
		sonIguales("01001", hijos[1]);
		
	}
	
	
	
	
	
	public void sonIguales(String bits, Individuo ind)
	{
		for(int i=0; i<bits.length(); ++i)
			assertEquals(bits.charAt(i) == '1', ind.get(i));
	}
	
	private Instancia defecto()
	{
		Instancia ret = new Instancia();
		
		ret.setCapacidad(10);
		ret.agregarObjeto(new Objeto("Linterna",   1, 7));
		ret.agregarObjeto(new Objeto("Carpa",      2, 5));
		ret.agregarObjeto(new Objeto("Bolso",      3, 3));
		ret.agregarObjeto(new Objeto("Calentador", 4, 3));
		ret.agregarObjeto(new Objeto("Notebook",   5, 200));
		
		return ret;
	}
	

}
