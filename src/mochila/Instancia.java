package mochila;

import java.util.ArrayList;

public class Instancia 
{
	private double capacidad;
	private ArrayList<Objeto> objetos;
	
	public Instancia()
	{
		objetos = new ArrayList<>();
	}
	
	public void setCapacidad(double capacidadMax)
	{
		capacidad = capacidadMax;
	}
	
	public double capacidad()
	{
		return capacidad;
	}
	
	public void agregarObjeto (Objeto obj)
	{
		objetos.add(obj);
	}
	
	public Objeto objeto(int i)
	{
		return objetos.get(i);
	}
	
	public ArrayList<Objeto> objetos() 
	{
		ArrayList<Objeto> ret = new ArrayList<Objeto>();
		
		for(int i=0; i<cantidadDeObjetos(); ++i)
			ret.add( objeto(i) );
		
		return ret;
	}
	
	public int cantidadDeObjetos()
	{
		return objetos.size();
	}	
}
