package mochila;

import java.util.ArrayList;
import java.util.Collections;

import interfaz.Observador;

public class Poblacion
{
	private ArrayList<Individuo> _individuos;
	private Instancia _instancia;
	private Generador _generador = new GeneradorRandom();
	private ArrayList<Observador> _observadores;
	
	private int _tama�o = 1000;
	private int _mutadosPorIteracion = 100;
	private int _combinacionesPorIteracion = 200;
	private int _eliminadosPorIteracion = 700;
	
	// Constructor
	public Poblacion(Instancia instancia)
	{
		_instancia = instancia;
		_observadores = new ArrayList<Observador>();
	}
	
	// La poblacion implementa Subject del pattern Observer
	public void registrarObservador(Observador observador)
	{
		_observadores.add(observador);
	}

	// Ejecuta la simulaci�n
	public Individuo simular()
	{
		generarAleatoriamente();
		while( satisfactoria() == false )
		{
			mutarAlgunos();
			recombinarAlgunos();
			eliminarPeores();
			completarConAleatorios();
			notificarObservadores();
		}
		
		return mejorIndividuo();		
	}

	// Pasos de cada iteracion de la simulaci�n
	private void generarAleatoriamente()
	{
		Individuo.setGenerador(_generador);
		_individuos = new ArrayList<Individuo>(2*_tama�o);
		
		for(int i=0; i<_tama�o; ++i)
			_individuos.add( Individuo.aleatorio(_instancia) );
	}
	private boolean satisfactoria()
	{
		return false;
	}
	private void mutarAlgunos()
	{
		for(int i=0; i<_mutadosPorIteracion; ++i)
			individuoAleatorio().mutar();
	}
	private void recombinarAlgunos()
	{
		for(int i=0; i<_combinacionesPorIteracion; ++i)
		{
			Individuo padre1 = individuoAleatorio();
			Individuo padre2 = individuoAleatorio();
		
			for(Individuo hijo: padre1.combinar(padre2))
				_individuos.add(hijo);
		}
	}
	private Individuo individuoAleatorio()
	{
		int i = _generador.nextInt( _individuos.size() );
		return _individuos.get(i);
	}
	private void eliminarPeores()
	{
		ordenar();
		for(int i=0; i<_eliminadosPorIteracion; ++i)
			_individuos.remove( _individuos.size()-1 );
	}
	private void completarConAleatorios()
	{
		while( _individuos.size() < _tama�o )
			_individuos.add( Individuo.aleatorio(_instancia) );
	}

	// Notificaci�n a los observadores
	private void notificarObservadores()
	{
		for(Observador observador: _observadores)
			observador.notificar();
	}

	// Para ser llamados desde los observadores
	public Individuo mejorIndividuo()
	{
		ordenar();
		return _individuos.get(0);
	}
	
	public Individuo peorIndividuo()
	{
		ordenar();
		return _individuos.get( _individuos.size()-1 );
	}
	
	public double fitnessPromedio()
	{
		double ret = 0;
		for(Individuo individuo: _individuos)
			ret += individuo.fitness();
		
		return ret / _individuos.size();
	}

	// Ordenamos de mayor a menor fitness
	private void ordenar()
	{
		Collections.sort(_individuos, (i,j) -> (int) (j.fitness() - i.fitness()));
	}
}
