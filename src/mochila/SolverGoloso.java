package mochila;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class SolverGoloso
{
	// Un solver est� siempre asociado con una instancia
	private Instancia _instancia;

	public SolverGoloso(Instancia instancia)
	{
		_instancia = instancia;
	}
	
	public Subconjunto resolver(Comparator<Objeto> comparador)
	{
		ArrayList<Objeto> ordenados = ordenar(comparador);
		return generarSolucion(ordenados);
	}

	private	ArrayList<Objeto> ordenar(Comparator<Objeto> comparador)
	{
		ArrayList<Objeto> ret = _instancia.objetos();
		Collections.sort(ret, comparador);
	 
		return ret;
	}

	private Subconjunto generarSolucion(ArrayList<Objeto> ordenados)
	{
		Subconjunto subconjunto = new Subconjunto(_instancia);
		for(Objeto obj: ordenados)
		{
			if( subconjunto.entra(obj) )
				subconjunto.addObjeto(obj);
		}

		return subconjunto;
	}
	
}